package no.uib.ii.inf112;

import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public interface TextAligner {

	/**
	 * Center a string. F.ex. assertEquals("  A  ", textUtils.center("A", 5));
	 *
	 * @param text  The string to be centered
	 * @param width The width of the page
	 * @return the centered text
	 */
	static String center(String text, int width){
		String result;

		if (width % 2 == 0) {                                                                   // If width is even
			if (text.length() % 2 == 0){
				int pad = (width-text.length())/2;
				result = " ".repeat(Math.max(0, pad)) + text + " ".repeat(Math.max(0, pad));
			} else {
				int pad = ((width-text.length())-1)/2;
				result = " ".repeat(Math.max(0, pad+1)) + text + " ".repeat(Math.max(0, pad));
			}
		} else {                                                                                // If width is odd
			if (text.length() % 2 == 0){
				int pad = ((width-text.length())-1)/2;
				result = " ".repeat(Math.max(0, pad+1)) + text + " ".repeat(Math.max(0, pad));
			} else {
				int pad = (width-text.length())/2;
				result = " ".repeat(Math.max(0, pad)) + text + " ".repeat(Math.max(0, pad));
			}
		}
		return result;
	}

	/**
	 * Align string to the right. F.ex. assertEquals("    A", textUtils.flushRight("A", 5));
	 * Pad the string with spaces on the right side using format().
	 * (-width) => Padding is on the right side og the string.
	 *
	 * @param text  The string to be aligned
	 * @param width The width of the page
	 * @return the aligned text
	 */
	static String flushRight(String text, int width){
		return String.format("%" + (-width) + "s", text);
	}

	/**
	 * Align string to the left. F.ex. assertEquals("A    ", textUtils.flushLeft("A", 5));
	 * Pad the string with spaces on the left side using format().
	 * width => Padding is on the right side of the string.
	 *
	 * @param text  The string to be aligned
	 * @param width The width of the page
	 * @return the aligned text
	 */
	static String flushLeft(String text, int width){
		return String.format("%" + width + "s", text);
	}

	/**
	 * Justify text, so it uses the whole width. It inserts extra spaces between words to make it fit the width.
	 * F.ex. assertEquals("fee   fie   foo", textUtils.justify("fee fie foo", 15));
	 *
	 * // @param text  The string to be aligned
	 * // @param width The width of the page
	 * // @return the aligned text
	 * @return New string with added spacing
	 */
	static String justify(StringBuilder text, int width) {

		//Turn StringBuilder into an arraylist of strings
		String[] words = text.toString().split(" ");
		List<String> arraylist = Arrays.asList(words);

		//Find the length of the words and length of the padding needed
		int lengthOfWords = 3 * arraylist.get(0).length();
		if (lengthOfWords > width){
			throw new AssertionError("The width is too small! Try making it bigger.");
		}
		int pad = width - lengthOfWords;

		//Add padding by using format on each word separately and then combine the strings and add last word
		String result;
		if (pad % 2 == 0){
			String temp1 = String.format("%" + (-(pad / 2) - 2) + "s", words[0]);
			String temp2 = String.format("%" + (-(pad / 2) - 2) + "s", words[0]);
			result = temp1 + temp2 + words[0];
		} else {
			pad--;
			String temp1 = String.format("%" + (-(pad / 2) - 2) + "s", words[0]);
			String temp2 = String.format("%" + (-(pad / 2) - 2) + "s", words[0]);
			result = temp1 + temp2 + words[0];

			//Turn string into arraylist
			ArrayList<Character> cs = new ArrayList<>();
			for (char c : result.toCharArray()){ cs.add(c); }

			//Add last whitespace between first and second word
			int index = nextSpaceIndex(cs);
			cs.add(index, ' ');

			//Turn arraylist back to string
			StringBuilder str = new StringBuilder();
			for (Character c : cs) { str.append(c); }
			result = str.toString();
		}
		return result;
	}

	/**
	 * Find the next space in arraylist of chars
	 * @param chars - an array of chars
	 * @return - the index of the first space in the arraylist
	 */
	static int nextSpaceIndex(@NotNull ArrayList<Character> chars) {
		int j = 0;
		while (j < chars.size()) {
			if (chars.get(j) == ' ') { return j; }
			j++;
		}
		return j;
	}
	static void main(String[] args) {
		// IO
		Scanner scanner = new Scanner(System.in);                       //Initialize scanner
		System.out.println("Please enter a string and an integer: ");   //Ask for input from user

		String line = scanner.nextLine();                               //Get input-string from user
		String[] strings = line.split(" ");                       //Split string at " " into list of strings
		String text = strings[0];                                       //Get first element from list as string
		int width = Integer.parseInt(strings[1]);                       //Get second element from list as an int

		// Create a string with three times the input text with spaces between
		StringBuilder forJustify = new StringBuilder();
		for(int i=0; i<3; i++){ forJustify.append(text).append(" "); }

		// Make sure the length of the string is less than the given int
		if (text == null || text.length() > width) {
			System.out.println(text);
			throw new IllegalArgumentException("Whoops! Invalid input. Try again.");
		}
		String flushR = "|" + flushRight(text, width)+ "|";
		String flushL = "|" + flushLeft(text, width)+ "|";
		String centered = "|" + center(text, width)+ "|";
		System.out.println("Right-padded:\t" + flushR + "\nLeft-padded:\t" + flushL + "\nCentered:\t\t" + centered);
		System.out.println("Justified:\t\t|" + justify(forJustify, width) + "|");
	}
}
